import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: number;
  upper: number;
  lower: number;

  constructor() {
    this.age = 0;
    this.upper = 0;
    this.lower = 0;

  }
  calculate() {
    this.upper = (220 - this.age) * 0.85;
    this.lower = (220 - this.age) * 0.65;
  }
}